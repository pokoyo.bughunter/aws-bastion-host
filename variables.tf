# Brainboard auto-generated file.

variable "debian_ami" {
  description = "Default Debian ami for region Frankfurt"
  type        = string
  default     = "ami-0adb6517915458bdb"
}

variable "ip" {
  description = "Authorized IP"
  type        = string
  default     = "0.0.0.0"
}

variable "public_key" {
  description = "Public key"
  type        = string
  default     = "default_key_pair.pem"
}

variable "key_name" {
  default     = "default_key_pair"
  description = "SSH key name in your AWS account for AWS instances."
}